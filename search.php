<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>
<div id="primary" class="content-area container">

  <div class="row d-none d-xl-block">
      <div class="col">

       
      </div>
    </div>
    <div class="row">
    <div class="col-12">


    </div>
  </div>


  <div class="row" style="color:white">
  <div class="col-12">
  <?php if ( have_posts() ) : ?>

<?php echo $wp_query->found_posts; ?> <?php _e( 'Search Results Found For', 'locale' ); ?>: "<?php the_search_query(); ?>"



<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>

<span class="search-post-title"> <a href="<?php the_permalink(); ?>"> <?php echo  the_title(); ?></a></span>
<?php
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
the_post_thumbnail( 'thumb' );
}
?>

<?php endwhile; ?>



<?php else : ?>

No Results

<?php endif; ?>
      </div>
    </div>







</div>


<?php get_footer(); ?>
