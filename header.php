<!DOCTYPE html>
<html lang="en">
<?php $image = get_field('header_image');?>
<?php $size = get_field('header_size');?>
<html <?php language_attributes();?> class="no-js">
<head>
<?php wp_head();?>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-title" content="Jenny Lind">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-57.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-72.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-114.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicons/favicon-180.png">
    <?php if (is_singular() && pings_open(get_queried_object())): ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url');?>">
    <?php endif;?>
</head><?php
$today = getdate();
$hour = ($today['hours']);
$hours = $hour + 1;
?>
<body class="sky-gradient sky-gradient-<?php print_r($hour)?>">
  <header>

  <div class="clock">  <?php
$today = getdate();
//print_r($today['hours'])+1;
print_r($hours);
?> oclock in Hastings</div>

      
      <?php if(function_exists('bcn_display'))
{
bcn_display();
}?>
        

<div class="container">
  <?php get_search_form();?>
</div>
    </header>
    <div class="content ">
