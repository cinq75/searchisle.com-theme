<!-- Default Single page template -->
<?php get_header(); ?>
<div id="primary" class="content-area container">

  <div class="row d-none d-xl-block">
      <div class="col">

     
        <!-- end breadcrumbs -->
      </div>
    </div>
    <div class="row">
    <div class="col-12">
  <h1> <?php the_title(); ?></h1>

  <div class="breadcrumbs">
  <?php if(function_exists('bcn_display'))
{
bcn_display();
}?>
</div>

    </div>
  </div>


  <div class="row">
  <div class="col-12">
            <?php
            // Start the loop.
            while (have_posts()) : the_post();
                ?>



                <?php
                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                    //the_post_thumbnail( 'full' );
                }
                ?>

                <?php
                the_content();
                ?>



            <?php
        // End of the loop.
        endwhile;
        ?>
      </div>
    </div>







</div>


<?php get_footer(); ?>
