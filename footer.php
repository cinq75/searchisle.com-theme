<div class="footer" >
<div class="waves">
<svg width="100%" height="200px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave"><defs></defs><path id="feel-the-wave" d=""/></svg>
<svg width="100%" height="200px" version="1.1" xmlns="http://www.w3.org/2000/svg" class="wave"><defs></defs><path id="feel-the-wave-two" d=""/></svg>
</div>
<div class="footer_text ">
<div class="container">
<?php get_sidebar('f1');?>
<?php get_sidebar('f2');?>
</div>
</div>
</div>
<?php wp_footer();?>
</body>
  </html>
