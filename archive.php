<?php get_header(); ?>
	<div id="primary" class="content-area container">
<div class="row">
	
<div class="col-12">
<h1><?php the_archive_title()?></h1>

</div>
</div>
  <div class="row">

		<?php
	    // Start the loop.
	      while (have_posts()) : the_post();
	            $thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
	$thumb_url = $thumb_url_array[0];
	    ?>

<?php $post_type = get_post_type_object(get_post_type($post));
	//var_dump($post_type);
	echo $post_type->label ; ?>	
<img src="<?php echo $thumb_url ?>" class="card-img-top img-fluid" alt="...">

<a href="<?php the_permalink() ?>" class="btn btn-primary"><h5 class="card-title"><?php the_title()?></h5></a>
    <?php the_excerpt()?>
  
  </div>
<?php
	// End of the loop.
	endwhile;
	?>

</div>
</div>
<?php get_footer(); ?>
