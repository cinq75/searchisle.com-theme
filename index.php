<?php get_header(); ?>
<div id="primary" class="content-area container">
    <div class="row">
        <div class="col-12">Breadcrumb
            <?php
                if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb('<p id="breadcrumb">', '</p>');
                }
                ?>
        </div>
        <div class="col-12">
            <h1>Blog</h1>
        </div>
    </div>
    <div class="row">
        <?php
        // Start the loop.
          while (have_posts()) : the_post();
                $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
    $thumb_url = $thumb_url_array[0];
        ?>

      
       
        <?php
    // End of the loop.
    endwhile;
    ?>
    </div>
</div>
<?php get_footer(); ?>
