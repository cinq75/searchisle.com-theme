<!-- mobile nav -->
<!-- The overlay -->
<div id="mobileNav">

	<!-- Overlay content -->
	<div class="overlay" id="overlay">
		<?php
		wp_nav_menu(array(
			'theme_location'    => 'mobile',
			'depth'             => 2,
			'menu_class'        => 'mobile',
			'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			'walker'            => new WP_Bootstrap_Navwalker(),
		));
		?>
		<div class="contact_overlay">Jenny Lind Inn | Hastings<br>
		  <a class="call_me" href="tel:+44-1424-421392"><i class="fas fa-phone"></i> 01424 421392</a>

	</div>
	</div>

</div>

<nav class="mobile-nav navbar navbar-expand-md navbar-light bg-light navbar-fixed-top d-xl-none" role="navigation">
	<div class="container ">
		<div class="row">
			<div class="col">

			<a class="logo_mobile" href="/" ></a>



				<div class="button_container" id="toggle"><span class="top"></span><span class="middle"></span><span class="bottom"></span></div>
			</div>
		</div>
	</div>
</nav>


<!-- desktop nav -->
<nav id="navy" class="navbar navbar-expand-md navbar-light bg-light navbar-fixed-top d-none d-xl-block" role="navigation">
	<div class="container navigation_top">
		<div class="top_left">
			<?php
			wp_nav_menu(array(
				'theme_location'    => 'menu_left',
				'depth'             => 2,
				'menu_class'        => 'menu_left',
				'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
				'walker'            => new WP_Bootstrap_Navwalker(),
			));
			?>
		</div>

			<a class="top_center" href="/" ></a>

		<div class="top_right">
			<?php
			wp_nav_menu(array(
				'theme_location'    => 'menu_right',
				'depth'             => 2,
				'menu_class'        => 'menu_right',
				'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
				'walker'            => new WP_Bootstrap_Navwalker(),
			));
			?>
		</div>
	</div>
	</div>
</nav>
