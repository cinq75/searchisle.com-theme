<?php function _themename_assets()
{
    //wp_enqueue_style( '_bootstrap-stylesheet', get_template_directory_uri() . '/dist/vendor/bootstrap/bootstrap.css', array(), '1.0.0', 'all' );//add boostrap
        wp_enqueue_style('_main-stylesheet', get_template_directory_uri() . '/style.min.css', array(), '1.0.0', 'all');
  wp_enqueue_script('_themename-scripts-jquery', get_template_directory_uri() . '/assets/js/vendor/jquery.min.js', array(), '1.0.0', false);
    wp_enqueue_script('_themename-scripts', get_template_directory_uri() . '/assets/js/vendor/bootstrap.js', array(), '1.0.0', true);
    wp_enqueue_script('_themename-scripts-fontawsome', 'https://use.fontawesome.com/releases/v5.7.1/js/all.js', array(), '1.0.0', true);
    wp_enqueue_script('_themename-scripts-cinq', get_template_directory_uri() . '/assets/js/cinq.js', array(), '1.0.0', true);

    wp_enqueue_script('_themename-scripts-cinq', get_template_directory_uri() . '/assets/js/cinq.js', array(), '1.0.0', true);

    
    wp_enqueue_script('_tween-cinq','https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js', array(), '1.0.0', true);
    wp_enqueue_script('_wavify-cinq','https://rawgit.com/peacepostman/wavify/master/wavify.js', array(), '', true);
    wp_enqueue_script('_wavify2-cinq','https://rawgit.com/peacepostman/wavify/master/jquery.wavify.js', array(), '', true);

       
    wp_enqueue_script('_waves-cinq', get_template_directory_uri() . '/assets/js//vendor/wave.js', array(), '1.0.0', true);
// just for full screen template
// Just add the code to the main call with the check

    if (is_page_template('template-contact.php')) {
        $mapcode = get_field('contact_address', 'option');
        wp_enqueue_script('_themename-googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCviv3yJZ4JI3RjRB-WaPYdppyWM91qu0U', array(), '', true);
        wp_enqueue_script('_themename-scripts-map', get_template_directory_uri() . '/assets/js/map.js', array(), '', true);
    }
}

add_action('wp_enqueue_scripts', '_themename_assets');
//ADD feature image support
add_theme_support('post-thumbnails');

require_once get_template_directory() . '/templates/function_searchform.php';
// Menus
require_once get_template_directory() . '/templates/functions_menu.php';
//Sidebars
require_once get_template_directory() . '/templates/functions_sidebars.php';
//Google Map api
function my_acf_init()
{
    acf_update_setting('google_api_key', 'AIzaSyCviv3yJZ4JI3RjRB-WaPYdppyWM91qu0U');
}
add_action('acf/init', 'my_acf_init');
?>